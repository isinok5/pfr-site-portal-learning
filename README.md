# PFR UI

Provide UI for pfr-portal-site

## Install npm packages

```bash
 npm install
```

## Run project

```bash
npm run start <host>
```

## build project to dist folder

```bash
npm run build
```

## Build docker image

```bash
 ./build_docker.sh
```

### docker run example

```bash
docker run -p 3000:80 -d pfr-portal-site:v18.26-23164c3
```

