const Webpack = require('webpack');
var fs = require('fs');
const Path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin');

const isProduction = process.argv.indexOf('-p') >= 0;
const outPath = Path.join(__dirname, './dist');
const sourcePath = Path.join(__dirname, './src');

var nodeModules = {};
fs.readdirSync('node_modules')
    .filter(function (x) {
        return ['.bin'].indexOf(x) === -1;
    })
    .forEach(function (mod) {
        nodeModules[mod] = 'commonjs ' + mod;
    });


function srcPath(subdir) {
    return Path.join(__dirname, "src", subdir);
}

module.exports = function (env) {
    const base = {
        mode: isProduction ? 'production' : 'development',
        context: sourcePath,
        entry: './server.tsx',
        output: {
            path: outPath,
            publicPath: '/',
            filename: 'server.js',

        },
        resolve: {
            alias: {
                app: srcPath('app'),
                assets: srcPath('assets'),
            },
            extensions: ['.js', '.ts', '.tsx'],
            mainFields: ['browser', 'main']
        },
        target: 'web',
        module: {
            rules: [
                // .ts, .tsx
                {
                    test: /\.tsx?$/,
                    use: isProduction
                        ? 'awesome-typescript-loader'
                        : [
                            'babel-loader',
                            'awesome-typescript-loader'
                        ]
                },
                // css
                {
                    test: /\.css$/,
                    use: [
                        'style-loader',
                        {
                            loader: 'css-loader',
                            query: {
                                modules: true,
                                sourceMap: !isProduction,
                                minimize: true,
                                importLoaders: 1,
                                localIdentName: '[name]__[local]___[hash:base64:5]'
                            }
                        },
                    ]
                },
                // svg
                {
                    test: /\.svg$/,
                    use: [
                        { loader: 'svg-sprite-loader' },
                    ]
                },

            ],
        },
        plugins: [
            new Webpack.DefinePlugin({
                // 'process.env.NODE_ENV': JSON.stringify('true'),
            }),
            new Webpack.optimize.AggressiveMergingPlugin(),
            new HtmlWebpackPlugin({
                template: 'index.html',
                // favicon: 'assets/icons/favicon.ico'
            }),
            new CopyWebpackPlugin([
                { from: 'assets', to: 'assets' },

            ]),
        ],
        devServer: {
            contentBase: sourcePath,
            hot: true,
            stats: {
                warnings: false
            },
        },
        node: {
            console: false,
            global: false,
            process: false,
            Buffer: false,
            __filename: false,
            __dirname: false
        }
    };

    base.entry = './index.tsx';
    base.output.filename = 'client.js';
    return base;
}
