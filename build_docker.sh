#!/usr/bin/env bash

# Version number
YEAR=`date +%y`
WEEK=`date +%V`
SHA=`git rev-parse --short HEAD`

# TODO: get build number from CI to change version in production build
VERSION="v$YEAR.$WEEK-$SHA"

IMAGE_NAME="pfr-portal-site:$VERSION"
echo ${IMAGE_NAME}
docker build -t ${IMAGE_NAME} -f ./Dockerfile .
#docker push ${IMAGE_NAME}