# build image
FROM node:10.5-alpine as builder

WORKDIR /app
COPY . ./

# install dependencies
RUN set -x \
    && npm install

## build
RUN set -x \
    && npm run build

# executable image
FROM nginx:1.15-alpine

# builded site
COPY --from=builder /app/dist /usr/share/nginx/html

CMD ["nginx", "-g", "daemon off;"]