/**
 * Developed by VIPCOR
 */

import { loggerMiddleware } from './logger';

export {
  loggerMiddleware,
};
