/**
 * Developed by VIPCOR
 */
export const loggerMiddleware = (_store) => next => action => {
      // tslint:disable-next-line:no-console
      console.log(action);
      return next(action);
};