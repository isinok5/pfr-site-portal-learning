/**
 * Developed by VIPCOR
 */

import * as React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';

import { Props, State } from './constants';
import { RootState } from 'app/reducers';
import * as style from './style.css';
import { PFRIcon } from 'app/components';
import { MyButton } from 'app/components';
import { MyInput } from 'app/components/MyInput';
import { MyTextarea } from 'app/components/MyTextarea';
import { any } from 'prop-types';

class Main extends React.Component<Props, State> {
    constructor(props: any) {
        super(props);

        this.state = {
            myObject: {
                name: any,
                surname: any,
                checkbox: any,
                hello: any,
                radio: any,
        
            }
        };
        
    }

    changFunc = (e, name) => {
        const myObject = this.state.myObject;
        myObject[name] = e.target.value;
        this.setState({myObject: myObject})
    }

    onBtnClickHandler = () => {
        let event = JSON.stringify(this.state.myObject);
        console.log(event);
    }

    render() {
        return (
            <div className={style.content}>
                <Helmet
                    title="PFR MAIN TITLE"
                />
                <PFRIcon name="notifications" color="gray"/>
                <MyInput name="Name" global_kind="text" local_kind="name" onChange={(e) =>this.changFunc(e, 'name')} />
                <MyInput name="Surname" global_kind="text" local_kind="surname" onChange={(e) =>this.changFunc(e, 'surname')} />
                <MyInput name="Checkbox" global_kind="checkbox" local_kind="checkbox" onChange={(e) =>this.changFunc(e, 'checkbox')} />
                <MyInput name="Radio" global_kind="radio" local_kind="radio" onChange={(e) =>this.changFunc(e, 'radio')} />
                <MyTextarea name="Hello" onChange={(e) =>this.changFunc(e, 'Hello')}/>
                <MyButton name="Ok" color="orange" onClick={this.onBtnClickHandler}/>

            </div>
        );
    }
}

function mapStateToProps(state: RootState) {
    const { } = state;
    return {
    };
}

function mapDispatchToProps(dispatch: any) {
    return {
        dispatch,
    };
}

const connectMain = connect(mapStateToProps, mapDispatchToProps)(Main);

export { connectMain as Main };