/**
 * Developed by VIPCOR
 */

export interface Props {
    route: any;
    dispatch: any;
}

export interface State {
    tmpVar?: any;
    myValue?: any;
    myObject?: {
        name: any,
        surname: any,
        checkbox: any,
        hello: any,
        radio: any,

    }
}

