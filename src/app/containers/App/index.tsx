/**
 * Developed by VIPCOR
 */
import * as React from 'react';
import { connect } from 'react-redux';
import { Switch, Route } from 'react-router';
import Helmet from 'react-helmet';
import * as i18next from 'i18next';

import { RootState } from 'app/reducers';
import { Routes } from 'app/routes';
import { PFRHeader } from 'app/components';
import { Props, State } from './constants';
import './style.css';

class App extends React.Component<Props, State> {

     constructor(props: any) {
        super(props);
        i18next.init({
            lng: 'ru',
            resources: require(`assets/i18n/ru.json`),
        });
    }


    render() {
        return (
            <div>
                <Helmet
                    title="PFR App Title"
                />
                <PFRHeader />
                <Switch>
                    {Routes().map((route) => <Route key={route.path} path={route.path} component={route.component} />)}
                </Switch>
            </div>
        );
    }
}

function mapStateToProps(state: RootState) {
    const { } = state;

    return {
    };
}

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
    };
}

const connectedApp = connect(mapStateToProps, mapDispatchToProps)(App);

export { connectedApp as App };
