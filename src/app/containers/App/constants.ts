/**
 * Developed by VIPCOR
 */
import { RouteComponentProps } from 'react-router';

export interface Props extends RouteComponentProps<void> {
    route: any;
    dispatch: any;
}

export interface State {
    tmpVar?: any;
}