/**
 * Developed by VIPCOR
 */

import { Main } from 'app/containers';

export const Routes = () =>  [
    {
        component: Main,
        exact: true,
        path: '/',
    },
];
