export interface Props {
    name?: string;
    className?: string;
    color: string;
    onClick?: any;
}
