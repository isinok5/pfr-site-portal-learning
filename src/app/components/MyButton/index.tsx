/**
 * Developed by Konstantin
 */

import * as React from 'react';
import * as style from './style.css';
import { Props } from './constants';

export class MyButton extends React.Component<Props> {
    constructor(props?: Props) {
        super(props);
    }
    
    render() {
        const { name, color, onClick } = this.props;
        return (
            <button onClick={onClick} className={style[`button-${color}`]}>{name}</button>
        );
    }
}