/**
 * Developed by VIPCOR
 */

export interface Props {
    title?: string;
}

export interface State {
    title?: string;
    isOpenSidebar: boolean;
}