/**
 * Developed by VIPCOR
 */

import * as React from 'react';
import * as i18next from 'i18next';

import * as style from './style.css';
import { Props, State } from './constants';
import { PFRIcon } from 'app/components/Icon';
import { PFRSidebar } from 'app/components';

export class PFRHeader extends React.Component<Props, State> {
    constructor(props?: Props, context?: any) {
        super(props, context);

        this.state = {
            isOpenSidebar: false,
            title: this.props.title,
        };

        this.openSidebar = this.openSidebar.bind(this);
    }

    openSidebar() {
        this.setState({ isOpenSidebar: !this.state.isOpenSidebar});
    }

    render() {
        return (
            <div>
                <PFRSidebar isOpen={this.state.isOpenSidebar} onChangeOpen={this.openSidebar} />
                <header className={style.header}>
                    <button onClick={this.openSidebar} className={style['header-menu__button']}>
                        <PFRIcon name="menu" color="white" className={style['header-menu__button__icon']} />
                    </button>
                    <div className={style['header-label']}>
                        <div className={style['header-label__logo']}>
                            <img className={style['header-label__logo__icon']} src="/assets/img/logo.png" />
                        </div>
                        <div className={style['header-label__versions']}>
                            <div className={style['header-label__versions-name']}>
                                {i18next.t('header.label.version.name')}
                            </div>
                            <div className={style['header-label__versions-description']}>
                                {i18next.t('header.label.version.description', { number: '4.4312' })}
                            </div>
                        </div>
                    </div>

                    <div className={style['header-title']}>
                        <div className={style['header-title__name']}>
                            {i18next.t('header.title.name')}
                        </div>
                        <div className={style['header-title__description']}>
                            {i18next.t('header.title.description')}
                        </div>
                    </div>

                    <div className={style['flex-spacer']}/>

                    <div className={style['header-notify']}>
                        <PFRIcon name="notifications" color="white" className={style['header-notify__icon']} />
                        <span className={style['header-notify__badge']}>3</span>
                    </div>
                    <div className={style['header-menu']}>
                        Иванов И.И. 
                    </div>
                </header>
            </div>
        );
    }
}
