/**
 * Developed by VIPCOR
 */

import * as React from 'react';
import 'assets/img/icons/icons.svg';

import * as style from './style.css';
import { Props, State } from './constants';

export class PFRIcon extends React.Component<Props, State> {
    constructor(props?: Props, context?: any) {
        super(props, context);
    }

    render() {

        const { name, color, ...rest } = this.props;

        return (
            <svg {...rest}>
                <use xlinkHref={`#icons_${name}`} className={style[`icon-${color}`]} />
            </svg>
        );

    }
}
