/**
 * Developed by VIPCOR
 */

export interface Props {
    name: string;
    className?: string;
    color: string;
}

export interface State {
    tmpVar?: any;
}

export const COLORS = {
    black: '#000000',
    blue: '#3745ad',
    gray: '#808080',
    white: '#ffffff',
};
