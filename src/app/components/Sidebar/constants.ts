/**
 * Developed by VIPCOR
 */

export interface Props {
    isOpen?: boolean;
    onChangeOpen?: any;
}

export interface State {
    isOpen?: boolean;
}