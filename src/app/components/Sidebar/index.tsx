/**
 * Developed by VIPCOR
 */

import * as React from 'react';
// import * as i18next from 'i18next';

import * as style from './style.css';
import { Props, State } from './constants';
import { PFRIcon } from 'app/components';

export class PFRSidebar extends React.Component<Props, State> {
    constructor(props?: Props, context?: any) {
        super(props, context);

        this.state = {
            isOpen: props.isOpen,
        };

        this.closeSidebar = this.closeSidebar.bind(this);
    }

    componentWillUpdate(props) {
        if (props.isOpen !== this.state.isOpen) {
            this.setState({ isOpen: props.isOpen});
        }
    }

    closeSidebar() {
        this.props.onChangeOpen(!this.state.isOpen);
    }

    render() {

        return (
            <div className={`${style.sidebar} ${this.state.isOpen ? style.sidebar__open : ''}`}>
                <div onClick={this.closeSidebar}
                    className={style['sidebar-overflow']}>
                </div>
                <div 
                className={`${style['sidebar-container']} ${style['sidebar-container__moving']}`}>
                    <div className={style['sidebar-header']}>
                        <button onClick={this.closeSidebar} className={style['sidebar-header__button']}>
                            <PFRIcon name="menu" color="white" className={style['sidebar-header__button__icon']} />
                        </button>
                    </div>
                    <div className={style['sidebar-body']}>
                        <ul className={style['sidebar-body-menu']}>
                            <li className={style['sidebar-body-menu__li']}>
                                <div className={style['sidebar-body__button__icon-container']}>
                                    <PFRIcon name="person" color="gray" className={style['sidebar-body__button__icon']} />
                                </div>
                                <span className={style['sidebar-body__button__span']}>lalala</span>
                            </li>
                            <li className={style['sidebar-body-menu__li']}>
                                <div className={style['sidebar-body__button__icon-container']}>
                                    <PFRIcon name="menu" color="gray" className={style['sidebar-body__button__icon']} />
                                </div>
                                <span className={style['sidebar-body__button__span']}>lalala</span>
                            </li>
                            <li className={style['sidebar-body-menu__li']}>
                                <div className={style['sidebar-body__button__icon-container']}>
                                    <PFRIcon name="person" color="gray" className={style['sidebar-body__button__icon']} />
                                </div>
                                <span className={style['sidebar-body__button__span']}>lalala</span>
                            </li>
                            <li className={style['sidebar-body-menu__li']}>
                                <div className={style['sidebar-body__button__icon-container']}>
                                    <PFRIcon name="person" color="gray" className={style['sidebar-body__button__icon']} />
                                </div>
                                <span className={style['sidebar-body__button__span']}>lalala</span>
                            </li>
                            <li className={style['sidebar-body-menu__li']}>
                                <div className={style['sidebar-body__button__icon-container']}>
                                    <PFRIcon name="person" color="gray" className={style['sidebar-body__button__icon']} />
                                </div>
                                <span className={style['sidebar-body__button__span']}>lalala</span>
                            </li>
                        </ul>
                    </div>
                    <div className={style['sidebar-footer']}>
                        footer
                    </div>
                </div>
            </div>
        );

    }
}
