export interface Props {
    name?: string;
    className?: string;
    onChange?: any;
    local_kind?: any;
    global_kind?: any;
}