/**
 * Developed by Konstantin
 */

import * as React from 'react';
import * as style from './style.css';
import { Props } from './constants';


export class MyInput extends React.Component<Props> {
    constructor(props?: Props) {
        super(props);
    }

    render() {
        
        const { name, onChange, local_kind, global_kind } = this.props;
        return (
            <input onChange={onChange} type={global_kind} className={style[`input-${local_kind}`]} placeholder={name}></input>

        );     
    }
}