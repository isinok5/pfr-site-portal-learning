/**
 * Developed by Konstantin
 */

import * as React from 'react';
import * as style from './style.css';
import { Props } from './constants';


export class MyTextarea extends React.Component<Props> {
    constructor(props?: Props) {
        super(props);
    }

    render() {
        
        const { name, onChange } = this.props;
        return (
            <textarea placeholder={name} onChange={onChange} className={style[`textarea-st`]}></textarea>

        );     
    }
}