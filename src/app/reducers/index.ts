/**
 * Developed by VIPCOR
 */
import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

export interface RootState {
    routing: any;
}

export const RootReducer =  combineReducers<RootState>({
    routing: routerReducer,
});